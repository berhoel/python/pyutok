"""Testing `utok`."""

import sys

from utok import prog, utok

__date__ = "2024/07/13 16:55:57 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


def test_do_not_change():
    """Testing nop for fine input."""
    assert utok(("/usr/man:/usr/local/man",)) == "/usr/man:/usr/local/man"


def test_do_not_change_space():
    """Testing nop for fine input with space delimiter."""
    assert (
        utok(("/usr/man /usr/local/man",), delimiter=" ") == "/usr/man /usr/local/man"
    )


def test_do_remove_double():
    """Testing removal of double entries."""
    assert utok(("/usr/man:/usr/man:/usr/local/man",)) == "/usr/man:/usr/local/man"


def test_do_remove_double_space():
    """Testing removal of double entries with space delimiter."""
    assert (
        utok(("/usr/man /usr/man /usr/local/man",), delimiter=" ")
        == "/usr/man /usr/local/man"
    )


def test_do_remove_double_2():
    """Testing removal of double entries."""
    assert utok(("/usr/man:/usr/local/man:/usr/man",)) == "/usr/man:/usr/local/man"


def test_do_remove_double_2_space():
    """Testing removal of double entries with space delimiter."""
    assert (
        utok(("/usr/man /usr/local/man /usr/man",), delimiter=" ")
        == "/usr/man /usr/local/man"
    )


def test_join_elements():
    """Testing joining of several entries."""
    assert (
        utok(
            (
                "/home/local/man",
                "/usr/local/man",
                "/usr/man:/usr/local/man",
                "/usr/openwin/man",
            ),
        )
        == "/home/local/man:/usr/local/man:/usr/man:/usr/openwin/man"
    )


def test_join_elements_space():
    """Testing joining of several entries with space delimiter."""
    assert (
        utok(
            (
                "/home/local/man",
                "/usr/local/man",
                "/usr/man /usr/local/man",
                "/usr/openwin/man",
            ),
            delimiter=" ",
        )
        == "/home/local/man /usr/local/man /usr/man /usr/openwin/man"
    )


def test_delete_one():
    """Testing deleting of entries."""
    assert (
        utok(("/usr/man:/usr/local/man",), delete_list="/usr/local/man") == "/usr/man"
    )


def test_delete_one_space():
    """Testing deleting of entries with space delimiter."""
    assert (
        utok(("/usr/man /usr/local/man",), delete_list="/usr/local/man", delimiter=" ")
        == "/usr/man"
    )


def test_join_elements_and_delete():
    """Testing joining and deleting of entries."""
    assert (
        utok(
            (
                "/home/local/man",
                "/usr/local/man",
                "/usr/man:/usr/local/man",
                "/usr/openwin/man",
            ),
            delete_list="/usr/local/man:/usr/openwin/man",
        )
        == "/home/local/man:/usr/man"
    )


def test_do_not_change_args(monkeypatch):
    """Testing nop for fine input with cmdline args."""
    testargs = ["utok", "/usr/man:/usr/local/man"]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man:/usr/local/man"


def test_do_not_change_space_args(monkeypatch):
    """Testing nop for fine input with space delimiter with cmdline args."""
    testargs = ["utok", "/usr/man /usr/local/man", "--delimiter", " "]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man /usr/local/man"


def test_do_remove_double_args(monkeypatch):
    """Testing removal of double entries with cmdline args."""
    testargs = ["utok", "/usr/man:/usr/man:/usr/local/man"]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man:/usr/local/man"


def test_do_remove_double_space_args(monkeypatch):
    """Testing removal of double entries with space delimiter with cmdline args."""
    testargs = ["utok", "/usr/man /usr/man /usr/local/man", "--delimiter= "]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man /usr/local/man"


def test_do_remove_double_2_args(monkeypatch):
    """Testing removal of double entries with cmdline args."""
    testargs = ["utok", "/usr/man:/usr/local/man:/usr/man"]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man:/usr/local/man"


def test_do_remove_double_2_space_args(monkeypatch):
    """Testing removal of double entries with space delimiter with cmdline args."""
    testargs = ["utok", "/usr/man /usr/local/man /usr/man", "--delimiter", " "]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man /usr/local/man"


def test_join_elements_args(monkeypatch):
    """Testing joining of several entries with cmdline args."""
    testargs = [
        "utok",
        "/home/local/man",
        "/usr/local/man",
        "/usr/man:/usr/local/man",
        "/usr/openwin/man",
    ]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/home/local/man:/usr/local/man:/usr/man:/usr/openwin/man"


def test_join_elements_space_args(monkeypatch):
    """Testing joining of several entries with space delimiter with cmdline args."""
    testargs = [
        "utok",
        "/home/local/man",
        "/usr/local/man",
        "/usr/man /usr/local/man",
        "/usr/openwin/man",
        "--delimiter",
        " ",
    ]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/home/local/man /usr/local/man /usr/man /usr/openwin/man"


def test_delete_one_args(monkeypatch):
    """Testing deleting of entries with cmdline args."""
    testargs = ["utok", "/usr/man:/usr/local/man", "--delete-list", "/usr/local/man"]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man"


def test_delete_one_space_args(monkeypatch):
    """Testing deleting of entries with space delimiter with cmdline args."""
    testargs = [
        "utok",
        "/usr/man /usr/local/man",
        "--delete-list",
        "/usr/local/man",
        "--delimiter",
        " ",
    ]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/usr/man"


def test_join_elements_and_delete_args(monkeypatch):
    """Testing joining and deleting of entries with cmdline args."""
    testargs = [
        "utok",
        "/home/local/man",
        "/usr/local/man",
        "/usr/man:/usr/local/man",
        "/usr/openwin/man",
        "--delete-list",
        "/usr/local/man:/usr/openwin/man",
    ]
    monkeypatch.setattr(sys, "argv", testargs)
    assert prog() == "/home/local/man:/usr/man"
