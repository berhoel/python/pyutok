..
  Task: Documentation of utok

  :Authors:
    - `Berthold Höllmann <berhoel@gmail.com>`__
  :Organization: Berthold Höllmann
  :datestamp: %Y-%m-%d
  :Copyright: Copyright © 2022 by Berthold Höllmann

============
utok command
============

.. sphinx_argparse_cli::
   :module: utok
   :func: get_parser
   :prog: utok


..
  Local Variables:
  mode: rst
  compile-command: "make html"
  coding: utf-8
  End:
