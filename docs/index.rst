.. include:: ../README.rst

API documentation
=================

.. currentmodule:: utok

.. autosummary::
   :nosignatures:

   utok

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   utok
   _tmp/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
