"""Configuration file for the Sphinx documentation builder."""

# For a full list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

project = "pyutok"
copyright = "2020, Berthold Höllmann <berhoel@gmail.com>"  # noqa:A001

# -- General configuration ---------------------------------------------------

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# -- Options for HTML output -------------------------------------------------

from berhoel.sphinx_settings import *  # isort:skip # noqa: E402, F403
from berhoel.sphinx_settings import configuration, extensions  # isort:skip # noqa: E402

globals().update(configuration().configuration())

extensions.extend(
    [
        "sphinx_argparse_cli",
    ],
)

# (Optional) Logo. Should be small enough to fit the navbar (ideally 24x24).
# Path should be relative to the ``_static`` files directory.
html_logo = "_static/my_logo.png"

html_static_path = ["_static"]

html_theme_options = {
    "navbar_links": [("GitLab", "https://gitlab.com/berhoel/python/pyutok", True)],
}

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
